# frozen_string_literal: true

require_relative File.expand_path('../lib/devops_labels.rb', __dir__)

Gitlab::Triage::Resource::Context.include DevopsLabels::Context
